#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "lsd.h"

int n_taxa;
int n_nodes;
int n_int_nodes;
int n_lines;
double **distances;

/*///////////////////////////////////////////////////////////////////*/
int getNew(struct Bracket par[],int n_int_nodes){
    int i;
    int val;
    val=0;
    for (i=n_int_nodes-1;i>=0;i--){
      //        if (par[i].opened == 1 && par[i].closed == 0){
      if (par[i].opened == 1){
            val=i+1;
            break;
        }
    }
    
    /*    for (i=n_int_nodes-1;i>0;i--){
        if (par[i].opened == 1 && par[i].closed == 1){
            val=i+1;
            break;
        }
	}*/
    
    return val;
}
/*///////////////////////////////////////////////////////////////////*/
int getLastToClose(struct Bracket par[],int n_int_nodes){
    int i;
    int val;
    val=-1;
    for (i=n_int_nodes-1;i>=0;i--){
        if (par[i].opened == 1 && par[i].closed == 0){
            val=i;
            break;
        }
    }
    return val;
}
/*///////////////////////////////////////////////////////////////////*/
int getLastClosed(struct Bracket par[],int n_int_nodes){
    int i;
    int val;
    val=-1;
    for (i=n_int_nodes-1;i>=0;i--){
        if (par[i].opened == 1 && par[i].closed == 1){
            val=i;
            break;
        }
    }
    return val;
}
/*///////////////////////////////////////////////////////////////////*/
void reset_node(struct Nodes *node){
    node->ancestor=-1;
    node->label="";
    node->dist=-1;
}
/*///////////////////////////////////////////////////////////////////*/
void update_treenode(struct Nodes *treenode, struct Nodes *node){
    treenode->ancestor=node->ancestor;
    int len=strlen(node->label) + 1;
    treenode->label = (char*)malloc(len);
    if (treenode->label == NULL){
        fprintf(stderr,"\tERROR: Error allocating memory\n");
        exit(1);
    }
    memset(treenode->label,'\0',len);
    strcpy(treenode->label,node->label);
    treenode->dist=node->dist;
}
/*///////////////////////////////////////////////////////////////////*/
void reset_string(char **string){
    (*string)=(char *) realloc((*string),1);
    if ((*string) == NULL){
        fprintf(stderr,"\tERROR: Error allocating memory\n");
        exit(1);
    }
    (*string)[0]='\0';
}
/*///////////////////////////////////////////////////////////////////*/
void reroot(struct Nodes *tree, struct Nodes *rerooted, char *outgroup){
    int i;

    for (i=0;i<n_nodes;i++){
        if (strcmp(tree[i].label,outgroup)==0){
            
            int ancestor=tree[i].ancestor;
            if(strcmp(tree[ancestor].label,"root")==0) /*the outgroup is already the outgroup*/
                break;
            
            double dist=(double)(tree[i].dist/(double)2.0); /*midpoint re-rooting*/
            
            /*new root*/
            rerooted[0].desc1=i;
            rerooted[0].desc2=ancestor;
            
            /*outgroup, I only change the affected properties FIXME*/
            rerooted[i].dist=dist;
            rerooted[i].ancestor=0;
            
            /*rest of the tree*/
            //rerooted[ancestor].dist=dist;

            int AC=i;
            int AN;
            do{
	      
                AN=tree[AC].ancestor;

                if (AC == i){
                    rerooted[AN].ancestor=0;
		    rerooted[AN].dist=dist;
                }else{
                    rerooted[AN].ancestor=AC;
		    rerooted[AN].dist=tree[AC].dist;
                }

                if (tree[AN].ancestor != 0){
                    rerooted[AN].desc2=tree[AN].ancestor;
                }else{
                    if (tree[0].desc1 != AN){
                        rerooted[AN].desc2=tree[0].desc1;
                        rerooted[tree[0].desc1].dist=tree[tree[0].desc2].dist+tree[tree[0].desc1].dist;
                        rerooted[tree[0].desc1].ancestor=AN;
                    }else{
                        rerooted[AN].desc2=tree[0].desc2;
                        rerooted[tree[0].desc2].dist=tree[tree[0].desc2].dist+tree[tree[0].desc1].dist;
                        rerooted[tree[0].desc2].ancestor=AN;
                    }
                }

                if (tree[AN].desc1 != AC){
                    rerooted[AN].desc1=tree[AN].desc1;
                }else{
                    rerooted[AN].desc1=tree[AN].desc2;
                }
		//printf("node=>%i, ancestor %i, desc1 %i, desc2 %i\n",AN,rerooted[AN].ancestor,rerooted[AN].desc1,rerooted[AN].desc2);
                AC=AN;

		
            }while(tree[AN].ancestor!=0);
	    
	    
            break;
        }
    }
}
/*///////////////////////////////////////////////////////////////////*/
int numberAnc(struct Nodes *tree,int elem){
    /*not used*/
    int tmp=elem;
    int n_anc=0;
    while(tmp != 0){
        tmp=tree[tmp].ancestor;
        n_anc++;
    }
    return n_anc;
}
/*///////////////////////////////////////////////////////////////////*/
int get_mrca(struct Nodes *tree,int *elem,int gsize){
    
    int **ancestors=(int**)malloc(sizeof(int *)*gsize);
    if (ancestors == NULL){
        fprintf(stderr,"\tERROR: Error allocating memory\n");
        exit(1);
    }
    int i,j,t;
    int cnt; /*counter for the number of ancestors that a leave has*/
    int cnt0; /*counter for the number of ancestors that the first valid leave has*/
    int mrca=0;
    
    /*fill vectors with the numbers that represent the succesive ancestors*/
    for (i = 0; i < gsize; i++){
        
        //int n_anc=numberAnc(tree,elem[i]);
        //ancestors[i]=(int *)malloc(sizeof(int)*(n_anc+1));
        ancestors[i]=(int *)malloc(sizeof(int));
        if (ancestors[i] == NULL){
            fprintf(stderr,"\tERROR: Error allocating memory\n");
            exit(1);
        }
        ancestors[i][0]=elem[i];
	//	printf("ELEM: %i->%i\n",i,elem[i]);
        cnt=1;
        
        while(ancestors[i][cnt-1] != 0){
            ancestors[i]=(int *)realloc(ancestors[i],sizeof(int)*(cnt+1));
            if (ancestors[i] == NULL){
                fprintf(stderr,"\tERROR: Error allocating memory\n");
                exit(1);
            }
            ancestors[i][cnt]=tree[ancestors[i][cnt-1]].ancestor;
	    //	    printf("\t%i descends from %i\n",ancestors[i][cnt-1],ancestors[i][cnt]);
            cnt++;
        }
        
        if (i == 0) cnt0=cnt;
    }
    
    /*traverse the vectors searching for the index that is present in all the vectors and is closest to the leaves*/
    for (j = 0; j < cnt0; j++){
        
        int first=ancestors[0][j];
        int b_ind,b_all;
        b_all=1;

        for (i = 1; i < gsize; i++){

            b_ind=0;
            t=0;
        
            do{
                if (ancestors[i][t] == first){b_ind=1;break;}
                t++;
            } while (ancestors[i][t-1] != 0);
            if (b_ind == 0){b_all=0;break;}

        }

        if (b_all == 1){mrca = first;break;}
        
    }
    
    for (i=0;i<gsize;i++)
        free(ancestors[i]);
    free(ancestors);
    
    return mrca;
}
/*///////////////////////////////////////////////////////////////////*/
void PairWiseDistances(struct Nodes *rerooted, double ***distances){
    int i,j;
    int mrca;
    int elem[2];
    int anc_end;
    int anc;
    int start;
    double dist;
    
    for (i = 0;i < n_nodes-1; i++){
        elem[0]=i;
        
        for (j = (i+1);j < n_nodes; j++){
            
            dist=0;
            elem[1]=j;
            mrca=get_mrca(rerooted,elem,2);
            
            if (mrca == i){anc_end=i;start=j;}
            else if (mrca == j){anc_end=j;start=i;}
            else {anc_end = mrca;start=i;}
            anc=start;

	    do{
	      dist+=rerooted[anc].dist;
	      anc=rerooted[anc].ancestor;
	      if ((*distances)[anc_end][anc] != -1){
		dist+=(*distances)[anc_end][anc];
		break;
	      }
	    } while(anc != anc_end);
            
            if (i != mrca && j != mrca){
                anc=j;
                do{
                    dist+=rerooted[anc].dist;
                    anc=rerooted[anc].ancestor;
		    if ((*distances)[anc_end][anc] != -1){
		      dist+=(*distances)[anc_end][anc];
		      break;
		    }
                }while (anc != anc_end);
            }
            //printf("Distance between %i %i, with mrca %i: %f\n=====================\n",i,j,mrca,dist);
            (*distances)[i][j]=dist;
            (*distances)[j][i]=(*distances)[i][j];
        }
        (*distances)[i][i]=0;
    }
}
/*///////////////////////////////////////////////////////////////////*/
void getIndexes(struct Nodes *rerooted,char **labels, int **Indexes, int size){
    /*returns the indexes of the rerooted array of Nodes that correspond to the list of strings passed in labels = maps names to vector indexes*/
    int j,t;
    
    for (j = 0; j < size; j++){
        (*Indexes)[j]=-1;
        for (t = 0; t < n_nodes; t++){
            if (strcmp(labels[j],rerooted[t].label) == 0){
                (*Indexes)[j]=t;
                break;
            }
        }
        if ((*Indexes)[j] == -1){
            fprintf(stderr,"\tERROR: %s is in the groups file, but not in the local gene tree\n",labels[j]);
            exit(1);
        }
    }
}
/*///////////////////////////////////////////////////////////////////*/
double AvDistGroupPair(int *Index1, int *Index2,int size1,int size2){
    int i,j;
    double val=0;
    for (i = 0; i < size1; i++)
        for (j = 0; j < size2; j++)
            val+=distances[Index1[i]][Index2[j]];
        
    return (double)(val/(double)(size1*size2));
}
/*///////////////////////////////////////////////////////////////////*/
double WithinA(int *Index,int size){
    int i,j;
    double val=0;
    for (i = 0; i < size-1; i++)
      for (j = (i+1); j < size; j++)
            val+=distances[Index[i]][Index[j]];
    
    return (double)(val/(double)(size*(size-1)));
}
/*///////////////////////////////////////////////////////////////////*/
double AvDistMRCA2Leaves(struct Nodes *rerooted,int *elem,int gsize,int mrca){
    
    int i;
    double sum=0;
    
    for (i = 0; i < gsize; i++){
      
        sum+=distances[elem[i]][mrca];
	//printf("DISTANCES => %i: %f\n",i,distances[elem[i]][mrca]);
    }
    return (double)(sum/(double)gsize);
}
/*///////////////////////////////////////////////////////////////////*/
void LSDcalc (struct Nodes *rerooted, struct Groups *groups, FILE *output_raw, FILE *output_norm){
    int i,j;
    double D_MRCA2A,D_MRCA2B,D_MRCA2A1,D_MRCA2A2,D_MRCA2B1,D_MRCA2B2;
    double k_AB,k_AB1,k_AB2,k_A1B,k_A2B,k_A1B1,k_A1B2,k_A2B1,k_A2B2;
    double d_A;
    
    distances=(double**)malloc(sizeof(double*)*n_nodes);
    if (distances == NULL){
        fprintf(stderr,"\tERROR: Error allocating memory\n");
        exit(1);
    }

    for (i = 0; i < n_nodes; i++){
        distances[i]=(double*)malloc(sizeof(double)*n_nodes);
        if (distances[i] == NULL){
            fprintf(stderr,"\tERROR: Error allocating memory\n");
            exit(1);
        }
    }
    for (i = 0; i < n_nodes; i++)
        for (j = 0; j < n_nodes; j++)
            distances[i][j]=-1;
    
    PairWiseDistances(rerooted, &distances);

    /*most of the time is wasted in recomputing k, which is a quadratic problem. 
      Here, I just store calculated values, in order to save time just in case they have to be re-calculated*/
    //    struct Between *k=(struct Groups*)malloc(sizeof(struct Groups));
    
    /*start for each line in groups file = for each target pop*/
    for (i = 0;i < n_lines-1;i++){
        double LSDden;
        double LSDnum;
        if (groups[i].A0size > 0 && groups[i].B0size > 0){ /*terminal lineages*/
           
           int sum=groups[i].A0size+groups[i].B0size;
           int *A0ind=(int*)malloc(groups[i].A0size*sizeof(int));
           int *B0ind=(int*)malloc(groups[i].B0size*sizeof(int));
           int *AB0ind=(int*)malloc(sum*sizeof(int));
           if (A0ind == NULL || B0ind == NULL || AB0ind == NULL){
                fprintf(stderr,"\tERROR: Error allocating memory\n");
                exit(1);
           }
           int last=groups[i].A0size;
           getIndexes(rerooted,groups[i].A0,&A0ind,groups[i].A0size);
           getIndexes(rerooted,groups[i].B0,&B0ind,groups[i].B0size);
           
           for (j = 0; j < groups[i].A0size; j++) AB0ind[j]=A0ind[j];
           for (j = last; j < sum; j++) AB0ind[j]=B0ind[j-last];
           
           int mrca=get_mrca(rerooted,AB0ind,sum);
           D_MRCA2A=AvDistMRCA2Leaves(rerooted,A0ind,groups[i].A0size,mrca);
           D_MRCA2B=AvDistMRCA2Leaves(rerooted,B0ind,groups[i].B0size,mrca);

	   
           k_AB=AvDistGroupPair(A0ind,B0ind,groups[i].A0size,groups[i].B0size);
           d_A=WithinA(A0ind,groups[i].A0size);

	   

           free(AB0ind);
           free(A0ind);
           free(B0ind);
           
        }else if (groups[i].A0size > 0 && groups[i].B0size == 0){ /*one terminal vs. one internal*/
            
            int sum=groups[i].A0size+groups[i].B1size+groups[i].B2size;
            int *A0ind=(int*)malloc(groups[i].A0size*sizeof(int));
            int *B1ind=(int*)malloc(groups[i].B1size*sizeof(int));
            int *B2ind=(int*)malloc(groups[i].B2size*sizeof(int));
            int *AB0ind=(int*)malloc(sum*sizeof(int));
            if (A0ind == NULL || B1ind == NULL || B2ind == NULL || AB0ind == NULL){
                fprintf(stderr,"\tERROR: Error allocating memory\n");
                exit(1);
            }
            int last=groups[i].A0size;
            
            getIndexes(rerooted,groups[i].A0,&A0ind,groups[i].A0size);
            getIndexes(rerooted,groups[i].B1,&B1ind,groups[i].B1size);
            getIndexes(rerooted,groups[i].B2,&B2ind,groups[i].B2size);
            
            for (j = 0; j < groups[i].A0size; j++) AB0ind[j]=A0ind[j];
            for (j = last; j < (last + groups[i].B1size); j++) AB0ind[j]=B1ind[j-last];
            last+=groups[i].B1size;
            for (j = last; j < sum; j++) AB0ind[j]=B2ind[j-last];
            
            int mrca=get_mrca(rerooted,AB0ind,sum);
            D_MRCA2A=AvDistMRCA2Leaves(rerooted,A0ind,groups[i].A0size,mrca);
            D_MRCA2B1=AvDistMRCA2Leaves(rerooted,B1ind,groups[i].B1size,mrca);
            D_MRCA2B2=AvDistMRCA2Leaves(rerooted,B2ind,groups[i].B2size,mrca);
            D_MRCA2B=(double)((D_MRCA2B1+D_MRCA2B2)/2.0);
            
            k_AB1=AvDistGroupPair(A0ind,B1ind,groups[i].A0size,groups[i].B1size);
            k_AB2=AvDistGroupPair(A0ind,B2ind,groups[i].A0size,groups[i].B2size);
            k_AB=(double)((k_AB1+k_AB2)/2.0);

	    d_A=WithinA(A0ind,groups[i].A0size);
            
            free(AB0ind);
            free(A0ind);
            free(B1ind);
            free(B2ind);
        }else if (groups[i].A0size == 0 && groups[i].B0size > 0){ /*one internal vs. one terminal*/
            int sum=groups[i].B0size+groups[i].A1size+groups[i].A2size;
            int *B0ind=(int*)malloc(groups[i].B0size*sizeof(int));
            int *A1ind=(int*)malloc(groups[i].A1size*sizeof(int));
            int *A2ind=(int*)malloc(groups[i].A2size*sizeof(int));
            int *AB0ind=(int*)malloc(sum*sizeof(int));
            if (B0ind == NULL || A1ind == NULL || A2ind == NULL || AB0ind == NULL){
                fprintf(stderr,"\tERROR: Error allocating memory\n");
                exit(1);
            }
            int last=groups[i].B0size;
            
            getIndexes(rerooted,groups[i].B0,&B0ind,groups[i].B0size);
            getIndexes(rerooted,groups[i].A1,&A1ind,groups[i].A1size);
            getIndexes(rerooted,groups[i].A2,&A2ind,groups[i].A2size);
            
            for (j = 0; j < groups[i].B0size; j++) AB0ind[j]=B0ind[j];
            for (j = last; j < (last + groups[i].A1size); j++) AB0ind[j]=A1ind[j-last];
            last+=groups[i].A1size;
            for (j = last; j < sum; j++) AB0ind[j]=A2ind[j-last];
            
            int mrca=get_mrca(rerooted,AB0ind,sum);
            D_MRCA2B=AvDistMRCA2Leaves(rerooted,B0ind,groups[i].B0size,mrca);
            D_MRCA2A1=AvDistMRCA2Leaves(rerooted,A1ind,groups[i].A1size,mrca);
            D_MRCA2A2=AvDistMRCA2Leaves(rerooted,A2ind,groups[i].A2size,mrca);
            D_MRCA2A=(double)((D_MRCA2A1+D_MRCA2A2)/2.0);
            
            k_A1B=AvDistGroupPair(B0ind,A1ind,groups[i].B0size,groups[i].A1size);
            k_A2B=AvDistGroupPair(B0ind,A2ind,groups[i].B0size,groups[i].A2size);
            k_AB=(double)((k_A1B+k_A2B)/2.0);

	    d_A=(double)(AvDistGroupPair(A1ind,A2ind,groups[i].A1size,groups[i].A2size)/(double)2.0);
            
            free(AB0ind);
            free(B0ind);
            free(A1ind);
            free(A2ind);
        }else if (groups[i].A0size == 0 && groups[i].B0size == 0){ /*internal lineages*/
            int sum=groups[i].B1size+groups[i].B2size+groups[i].A1size+groups[i].A2size;
            int *A1ind=(int*)malloc(groups[i].A1size*sizeof(int));
            int *A2ind=(int*)malloc(groups[i].A2size*sizeof(int));
            int *B1ind=(int*)malloc(groups[i].B1size*sizeof(int));
            int *B2ind=(int*)malloc(groups[i].B2size*sizeof(int));
            int *AB0ind=(int*)malloc(sum*sizeof(int));
            if (B1ind == NULL || B2ind == NULL || A1ind == NULL || A2ind == NULL || AB0ind == NULL){
                fprintf(stderr,"\tERROR: Error allocating memory\n");
                exit(1);
            }
            int last=groups[i].A1size;
            
            getIndexes(rerooted,groups[i].B1,&B1ind,groups[i].B1size);
            getIndexes(rerooted,groups[i].B2,&B2ind,groups[i].B2size);
            getIndexes(rerooted,groups[i].A1,&A1ind,groups[i].A1size);
            getIndexes(rerooted,groups[i].A2,&A2ind,groups[i].A2size);
            
            for (j = 0; j < groups[i].A1size; j++) AB0ind[j]=A1ind[j];
            for (j = last; j < (last + groups[i].A2size); j++) AB0ind[j]=A2ind[j-last];
            last+=groups[i].A2size;
            for (j = last; j < (last + groups[i].B1size); j++) AB0ind[j]=B1ind[j-last];
            last+=groups[i].B1size;
            for (j = last; j < sum; j++) AB0ind[j]=B2ind[j-last];
            
            int mrca=get_mrca(rerooted,AB0ind,sum);
            D_MRCA2B1=AvDistMRCA2Leaves(rerooted,B1ind,groups[i].B1size,mrca);
            D_MRCA2B2=AvDistMRCA2Leaves(rerooted,B2ind,groups[i].B2size,mrca);
            D_MRCA2A1=AvDistMRCA2Leaves(rerooted,A1ind,groups[i].A1size,mrca);
            D_MRCA2A2=AvDistMRCA2Leaves(rerooted,A2ind,groups[i].A2size,mrca);
            D_MRCA2A=(double)((D_MRCA2A1+D_MRCA2A2)/2.0);
            D_MRCA2B=(double)((D_MRCA2B1+D_MRCA2B2)/2.0);
            
            k_A1B1=AvDistGroupPair(B1ind,A1ind,groups[i].B1size,groups[i].A1size);
            k_A2B1=AvDistGroupPair(B1ind,A2ind,groups[i].B1size,groups[i].A2size);
            k_A1B2=AvDistGroupPair(B2ind,A1ind,groups[i].B2size,groups[i].A1size);
            k_A2B2=AvDistGroupPair(B2ind,A2ind,groups[i].B2size,groups[i].A2size);
            k_AB=(double)((k_A1B1+k_A1B2+k_A2B1+k_A2B2)/4.0);
            
	    d_A=(double)(AvDistGroupPair(A1ind,A2ind,groups[i].A1size,groups[i].A2size)/(double)2.0);

            free(AB0ind);
            free(B1ind);
            free(B2ind);
            free(A1ind);
            free(A2ind);
            
        }
        
        LSDden=(double)((D_MRCA2A+k_AB-D_MRCA2B)/2.0);
        LSDnum=LSDden-d_A;
        fprintf(output_raw,"%.10f\t",LSDnum);
        if (LSDden>0){
            fprintf(output_norm,"%.10f\t",((double)LSDnum/LSDden));
        }else{
            fprintf(output_norm,"NA\t");
        }
    }
    fprintf(output_raw,"\n");
    fprintf(output_norm,"\n");
    
    for (i = 0; i < n_nodes; i++)
        free(distances[i]);
    free(distances);
}
/*///////////////////////////////////////////////////////////////////*/
void read_nwk(char *filename,struct Groups *groups,char *root, char *outputname){
    
    FILE *nwk;
    FILE *output_raw;
    FILE *output_norm;
    
    char *line;
    char *dist;
    char *label;
    
    unsigned long cnt_trees=0;
    int is_dist=0;
    size_t len=100000;
    
    dist=(char *) malloc(1);
    label=(char *) malloc(1);
    if (dist == NULL || label==NULL){
        fprintf(stderr,"\tERROR: Error allocating memory\n");
        exit(1);
    }
    
    dist[0]='\0';
    label[0]='\0';
    
    nwk=fopen(filename,"r");
    if (nwk == NULL){
        fprintf(stderr,"\tERROR: Unable to open %s\n",filename);
        exit(1);
    }
    
    
    char *rawname=malloc(strlen(outputname)+5);
    if (rawname == NULL){
        fprintf(stderr,"\tERROR: Error allocating memory\n");
        exit(1);
    }
    memset(rawname,'\0',strlen(outputname)+5);
    strcpy(rawname,outputname);
    strncat(rawname,"_raw",4);
    output_raw=fopen(rawname,"w");
    if (output_raw == NULL){
        fprintf(stderr,"\tERROR: Unable to open %s for writing\n",rawname);
        exit(1);
    }
    
    
    char *normname=malloc(strlen(outputname)+6);
    if (normname == NULL){
        fprintf(stderr,"\tERROR: Error allocating memory\n");
        exit(1);
    }
    memset(normname,'\0',strlen(outputname)+6);
    strcpy(normname,outputname);
    strncat(normname,"_norm",5);
    output_norm=fopen(normname,"w");
    if (output_norm == NULL){
        fprintf(stderr,"\tERROR: Unable to open %s for writing\n",normname);
        exit(1);
    }
    
    
    line = (char*)malloc(len);
    if (line == NULL){
      fprintf(stderr,"\tERROR: Cannot allocate memory to read a newick tree\n");
      exit(1);
    }
    memset(line,'\0',len);

    while (getline(&line, &len, nwk) != -1) {
      
            struct Bracket par[n_int_nodes];
            struct Nodes tree[n_nodes];
            struct Nodes rerooted[n_nodes];
            struct Nodes cnode;
        
            unsigned long i;
            int par_n;
            int elem;
            int last_closed=-1;
            int penultimate_closed=-1;
            for (i=0;i<n_int_nodes;i++){
                par[i].opened=0;
                par[i].closed=0;
            }
        
            for (i=0;i<n_nodes;i++){
                reset_node(&tree[i]);
                reset_node(&rerooted[i]);
                tree[i].desc1=-1;
                tree[i].desc2=-1;
                rerooted[i].desc1=-1;
                rerooted[i].desc2=-1;
            }
            reset_node(&cnode);
            elem=n_int_nodes;
            i=0;
            tree[0].label="root";
            rerooted[0].label="root";
            cnode=tree[0];
        
            for (i = 0; i < strlen(line); i++){
	      //printf("%lu: %c = %i %i \n",i,line[i],tree[0].desc1,tree[0].desc2);
	      switch (line[i]){
                    case ' ':{
                    }; break;
                    case '\n':{
                    }; break;
                    case '\r':{
                    }; break;
                    case '(': {
                        is_dist=0;
                        par_n=getNew(par,n_int_nodes);
                        par[par_n].opened=1;
			//printf("\tOpenning %i\n",par_n);
                    }; break;
                    case ')':
                    case ',': {
                        if (dist[0] == '\0'){
                            fprintf(stderr,"\tERROR: Node lacking distance in local gene tree %s\n",line);
                            exit(1);
                        }
                            
                        par_n=getLastToClose(par,n_int_nodes);
                        if (tree[par_n].desc1 != -1 && tree[par_n].desc2 != -1){ /*check if tree is multifurcating*/
             
                            /*creates a new "fake" ancestor node of distance 0 to force bifurcation*/
                            int newAnc=getNew(par,n_int_nodes);
                            par[newAnc].opened=1;
                            par[newAnc].closed=1;

			    /*printf("Tree %i and %i are going together, descending from %i\n", tree[par_n].desc1,tree[par_n].desc2,newAnc);*/
                            
                            rerooted[newAnc].label=(char*)malloc(1);
                            tree[newAnc].label=(char*)malloc(1);
                            if (tree[newAnc].label == NULL || rerooted[newAnc].label == NULL){
                                fprintf(stderr,"\tERROR: Error allocating memory\n");
                                exit(1);
                            }
                            tree[newAnc].dist=0.0;
                            tree[newAnc].label[0]='\0';
                            rerooted[newAnc].dist=0.0;
                            rerooted[newAnc].label[0]='\0';
                            

                            
                            /*the two prior descendants will have this new fake  ancestor*/
                            tree[tree[par_n].desc1].ancestor=newAnc;
                            tree[tree[par_n].desc2].ancestor=newAnc;
                            rerooted[tree[par_n].desc1].ancestor=newAnc;
                            rerooted[tree[par_n].desc2].ancestor=newAnc;
                            
                            tree[newAnc].desc1=tree[par_n].desc1;
                            tree[newAnc].desc2=tree[par_n].desc2;
                            rerooted[newAnc].desc1=tree[par_n].desc1;
                            rerooted[newAnc].desc2=tree[par_n].desc2;
                            
                            /*point this new "fake" ancestor node as desc1 of par_n*/
                            tree[par_n].desc1=newAnc;
                            tree[par_n].desc2=-1;
                            rerooted[par_n].desc1=newAnc;
                            rerooted[par_n].desc2=-1;

                            /*and vice versa*/
                            tree[newAnc].ancestor=par_n;
                            rerooted[newAnc].ancestor=par_n;
                            
                            
                        }
                        
                        if (line[i] == ')'){
                            par[par_n].closed=1;
                            penultimate_closed=last_closed;
                            last_closed=par_n;
			    //printf("\tClosing %i\n",par_n);
                        }
                        
                        /*information collected for every kind of node*/
                        cnode.ancestor=par_n;
                        cnode.dist=atof(dist);
                        reset_string(&dist);
                    
                        /*information for tip nodes*/
                        if (label[0] != '\0'){
                            int labelen=strlen(label) + 1;
                            cnode.label = (char*)malloc(labelen);
                            if (cnode.label == NULL){
                                fprintf(stderr,"\tERROR: Error allocating memory\n");
                                exit(1);
                            }
                            memset(cnode.label,'\0',labelen);
                            strcpy(cnode.label,label);
			    //printf("updating node: %i\n",elem);
                            update_treenode(&tree[elem],&cnode);
                            update_treenode(&rerooted[elem],&cnode);

                            if (tree[par_n].desc1 == -1){ /*in the tip nodes, I update the ancestor node for desc1 and desc2*/
                                tree[par_n].desc1=elem;
                                rerooted[par_n].desc1=elem;
                            }else if (tree[par_n].desc2 == -1){
                                tree[par_n].desc2=elem;
                                rerooted[par_n].desc2=elem;
                            }

                            elem++;
                            reset_string(&label);
                            free(cnode.label);
                        }else{ /*information for internal nodes*/

                            int update;
                            if (line[i] == ','){
                                update=last_closed;
                            }else{
                                /*if it comes here, it means that the tree has two consecutive ")". Thus, the node to be updated is not the last_closed, but the previous one*/
                                update=penultimate_closed;
                            }
			    //printf("last closed (updating node): %i\n",update);
                            update_treenode(&tree[update],&cnode);
                            update_treenode(&rerooted[update],&cnode);
                            if (tree[par_n].desc1 == -1){
                                tree[par_n].desc1=update;
                                rerooted[par_n].desc1=update;
                            }else if (tree[par_n].desc2 == -1){
                                tree[par_n].desc2=update;
                                rerooted[par_n].desc2=update;
                            }
                        }
                        is_dist=0;
                    
                        reset_node(&cnode);

                    }; break;
                    case ':': {
                        is_dist=1;
                    }; break;
                    case ';': {
                    }; break;
                    default :{
                        if (is_dist == 1){
                            if (isdigit(line[i]) ||line[i] == '.'){	
                                dist=(char *)realloc(dist,strlen(dist)+2);
                                if (dist == NULL){
                                    fprintf(stderr,"\tERROR: Error allocating memory\n");
                                    exit(1);
                                }
                                strncat(dist,&line[i],1);
                            }else{
                                fprintf(stderr,"\tERROR: Non-numeric character in distance %c\n",line[i]);
                                exit(1);
                            }
                        }else{
                            label=(char *)realloc(label,strlen(label)+2);
                            if (label == NULL){
                                fprintf(stderr,"\tERROR: Error allocating memory\n");
                                exit(1);
                            }
                            strncat(label,&line[i],1);
                        }
                    }; break;
                }
            }
            int j;
        
            for (j=0;j<n_int_nodes;j++){
                if (par[j].opened != 1 || par[j].closed != 1){
                    fprintf(stderr,"\tERROR: Problem with paranthesis in %s, or with the indicated number of taxa (%i)\n",line,n_taxa);
                    exit(1);
                }
            }

	    /* //only for debugging tree structure */
        
/*            for (j=0;j<n_nodes;j++)
                printf("NODE %i: LABEL(%s) ANCESTOR(%i) DIST(%f) DESC1(%i) DESC2(%i)\n",j,tree[j].label,tree[j].ancestor,tree[j].dist,tree[j].desc1,tree[j].desc2);
		printf("====================\n");*/
	reroot(tree,rerooted,root); 
	/*            for (j=0;j<n_nodes;j++)
		      printf("NODE %i: LABEL(%s) ANCESTOR(%i) DIST(%f) DESC1(%i) DESC2(%i)\n",j,rerooted[j].label,rerooted[j].ancestor,rerooted[j].dist,rerooted[j].desc1,rerooted[j].desc2);*/

        LSDcalc(rerooted,groups,output_raw,output_norm);
        if (cnt_trees == 0 || cnt_trees % 1000 == 0){
            fprintf(stderr,"\tTree %lu processed\n",cnt_trees);
        }
        cnt_trees++;
        
        for (j=1;j<n_nodes;j++){ 
	      free(tree[j].label);
	      free(rerooted[j].label);
        }
            
    }
    fclose(nwk);
    fclose(output_raw);
    fclose(output_norm);
    free(line);
    free(dist);
    free(label);

}
/*///////////////////////////////////////////////////////////////////*/
char *removeSpace(char *string){
    char* clean=(char*)malloc(strlen(string)+1);
    int i = 0;
    int j = 0;
    while (string[i] != '\0') {
        if (string[i] != ' '){
            clean[j]=string[i];
            j++;
        }
        i++;
    }
    clean[j]='\0';
    return clean;
}
/*///////////////////////////////////////////////////////////////////*/
int readgroups2(char *groupname, struct Groups **groups,char ***header){
    
    size_t len=100000;
    char *line;
    int cnt;
    int n_fields;
    
    n_lines=0;
    FILE *grp;
    
    grp=fopen(groupname,"r");
    if (grp == NULL){
        fprintf(stderr,"\tERROR: Unable to open %s\n",groupname);
        exit(1);
    }
    
    line = (char*)malloc(len);
    if (line == NULL){
      fprintf(stderr,"\tERROR: Cannot allocate memory to read a group\n");
      exit(1);
    }
    memset(line,'\0',len);

    while (getline(&line, &len, grp) != -1) {
        if (line[0]=='\n' || line[0]=='\r' || line[0]=='\0')
            continue;
        
        line[strlen(line)-1]='\0';
        if (line[strlen(line)-1] == '\r')
            line[strlen(line)-1]='\0';
        
        char *field;
        int fieldlen;
        if (n_lines == 0){ /*header*/
            cnt=1;
            field = strtok(line, "\t");
            fieldlen=strlen(field)+1;
            (*header)[0]=(char *)malloc(sizeof(char)*(fieldlen));
            if ((*header)[0] == NULL){
                fprintf(stderr,"\tERROR: Error allocating memory\n");
                exit(1);
            }
            memset((*header)[0],'\0',fieldlen);
            strcpy((*header)[0],field);

            while (field != NULL){
                field = strtok (NULL, "\t");
                if (field == NULL)
                    break;
                
                char *field_clean=removeSpace(field);
                
                (*header)=(char **)realloc((*header),sizeof(char **)*(cnt+1));
                if ((*header) == NULL){
                    fprintf(stderr,"\tERROR: Error allocating memory\n");
                    exit(1);
                }
                fieldlen=strlen(field_clean)+1;
                (*header)[cnt]=(char *)malloc(sizeof(char)*fieldlen);
                if ((*header)[cnt] == NULL){
                    fprintf(stderr,"\tERROR: Error allocating memory\n");
                    exit(1);
                }
                memset((*header)[cnt],'\0',fieldlen);
                strcpy((*header)[cnt],field_clean);
                cnt++;
            }
            n_fields=cnt;
        }else{ /*group data*/
	    int n_lines2=n_lines-1;
            (*groups)=(struct Groups*)realloc((*groups),sizeof(struct Groups)*(n_lines));
            if ((*groups) == NULL){
                fprintf(stderr,"\tERROR: Error allocating memory\n");
                exit(1);
            }
            (*groups)[n_lines2].A0size=0;
            (*groups)[n_lines2].A1size=0;
            (*groups)[n_lines2].A2size=0;
            (*groups)[n_lines2].B0size=0;
            (*groups)[n_lines2].B1size=0;
            (*groups)[n_lines2].B2size=0;
            
            cnt=0;
            field = strtok(line, "\t");
            do{
                if (strcmp(field,"A0")==0){
                    if ((*groups)[n_lines2].A0size >0 ){
                        (*groups)[n_lines2].A0=(char **)realloc((*groups)[n_lines2].A0,sizeof(char*)*((*groups)[n_lines2].A0size+1));
                    }else{
                        (*groups)[n_lines2].A0=(char **)malloc(sizeof(char*)*((*groups)[n_lines2].A0size+1));
                    }
                    if ((*groups)[n_lines2].A0 == NULL){
                        fprintf(stderr,"\tERROR: Error allocating memory\n");
                        exit(1);
                    }
                    fieldlen=strlen((*header)[cnt])+1;
                    (*groups)[n_lines2].A0[(*groups)[n_lines2].A0size]=(char *)malloc(fieldlen);
                    memset((*groups)[n_lines2].A0[(*groups)[n_lines2].A0size],'\0',fieldlen);
                    strcpy((*groups)[n_lines2].A0[(*groups)[n_lines2].A0size],(*header)[cnt]);
                    (*groups)[n_lines2].A0size++;
                }else if (strcmp(field,"A1")==0){
                    if ((*groups)[n_lines2].A1size >0 ){
                        (*groups)[n_lines2].A1=(char **)realloc((*groups)[n_lines2].A1,sizeof(char*)*((*groups)[n_lines2].A1size+1));
                    }else{
                        (*groups)[n_lines2].A1=(char **)malloc(sizeof(char*)*((*groups)[n_lines2].A1size+1));
                    }
                    if ((*groups)[n_lines2].A1 == NULL){
                        fprintf(stderr,"\tERROR: Error allocating memory\n");
                        exit(1);
                    }
                    fieldlen=strlen((*header)[cnt])+1;
                    (*groups)[n_lines2].A1[(*groups)[n_lines2].A1size]=(char *)malloc(fieldlen);
                    memset((*groups)[n_lines2].A1[(*groups)[n_lines2].A1size],'\0',fieldlen);
                    strcpy((*groups)[n_lines2].A1[(*groups)[n_lines2].A1size],(*header)[cnt]);
                    (*groups)[n_lines2].A1size++;
                }else if (strcmp(field,"A2")==0){
                    if ((*groups)[n_lines2].A2size >0 ){
                        (*groups)[n_lines2].A2=(char **)realloc((*groups)[n_lines2].A2,sizeof(char*)*((*groups)[n_lines2].A2size+1));
		    }else{
                        (*groups)[n_lines2].A2=(char **)malloc(sizeof(char*)*((*groups)[n_lines2].A2size+1));
                    }
                    if ((*groups)[n_lines2].A2 == NULL){
                        fprintf(stderr,"\tERROR: Error allocating memory\n");
                        exit(1);
                    }
		    fieldlen=strlen((*header)[cnt])+1;
                    (*groups)[n_lines2].A2[(*groups)[n_lines2].A2size]=(char *)malloc(fieldlen);
                    memset((*groups)[n_lines2].A2[(*groups)[n_lines2].A2size],'\0',fieldlen);
                    strcpy((*groups)[n_lines2].A2[(*groups)[n_lines2].A2size],(*header)[cnt]);
                    (*groups)[n_lines2].A2size++;
                }else if (strcmp(field,"B0")==0){
                    if ((*groups)[n_lines2].B0size >0 ){
                        (*groups)[n_lines2].B0=(char **)realloc((*groups)[n_lines2].B0,sizeof(char*)*((*groups)[n_lines2].B0size+1));
                    }else{
                        (*groups)[n_lines2].B0=(char **)malloc(sizeof(char*)*((*groups)[n_lines2].B0size+1));
                    }
                    if ((*groups)[n_lines2].B0 == NULL){
                        fprintf(stderr,"\tERROR: Error allocating memory\n");
                        exit(1);
                    }
                    fieldlen=strlen((*header)[cnt])+1;
                    (*groups)[n_lines2].B0[(*groups)[n_lines2].B0size]=(char *)malloc(fieldlen);
                    memset((*groups)[n_lines2].B0[(*groups)[n_lines2].B0size],'\0',fieldlen);
                    strcpy((*groups)[n_lines2].B0[(*groups)[n_lines2].B0size],(*header)[cnt]);
                    (*groups)[n_lines2].B0size++;
                }else if (strcmp(field,"B1")==0){
                    if ((*groups)[n_lines2].B1size >0 ){
                        (*groups)[n_lines2].B1=(char **)realloc((*groups)[n_lines2].B1,sizeof(char*)*((*groups)[n_lines2].B1size+1));
                    }else{
                        (*groups)[n_lines2].B1=(char **)malloc(sizeof(char*)*((*groups)[n_lines2].B1size+1));
                    }
                    if ((*groups)[n_lines2].B1 == NULL){
                        fprintf(stderr,"\tERROR: Error allocating memory\n");
                        exit(1);
                    }
                    fieldlen=strlen((*header)[cnt])+1;
                    (*groups)[n_lines2].B1[(*groups)[n_lines2].B1size]=(char *)malloc(fieldlen);
                    memset((*groups)[n_lines2].B1[(*groups)[n_lines2].B1size],'\0',fieldlen);
                    strcpy((*groups)[n_lines2].B1[(*groups)[n_lines2].B1size],(*header)[cnt]);
                    (*groups)[n_lines2].B1size++;
                }else if (strcmp(field,"B2")==0){
                    if ((*groups)[n_lines2].B2size >0 ){
                        (*groups)[n_lines2].B2=(char **)realloc((*groups)[n_lines2].B2,sizeof(char*)*((*groups)[n_lines2].B2size+1));
                    }else{
                        (*groups)[n_lines2].B2=(char **)malloc(sizeof(char*)*((*groups)[n_lines2].B2size+1));
                    }
                    if ((*groups)[n_lines2].B2 == NULL){
                        fprintf(stderr,"\tERROR: Error allocating memory\n");
                        exit(1);
                    }
                    fieldlen=strlen((*header)[cnt])+1;
                    (*groups)[n_lines2].B2[(*groups)[n_lines2].B2size]=(char *)malloc(fieldlen);
                    memset((*groups)[n_lines2].B2[(*groups)[n_lines2].B2size],'\0',fieldlen);
                    strcpy((*groups)[n_lines2].B2[(*groups)[n_lines2].B2size],(*header)[cnt]);
                    (*groups)[n_lines2].B2size++;
                }else if (strcmp(field,"00")==0){
                    
                }else{
                    fprintf(stderr,"\tERROR: Symbol in group table is not 00, A0, A1, A2, B0, B1 or B2. Line  %s, field \"%s\" (%i)\n",line,field,cnt);
                    exit(1);
                }
                cnt++;
                field = strtok (NULL, "\t");
            }while (field != NULL);
        }
        n_lines++;
        
    }
    fclose(grp);
    free(line);
    return n_fields;
}
/*///////////////////////////////////////////////////////////////////*/
void usage(void)
{
    printf(LSD);
    printf("\nUsage of LSD:\n");
    
    printf("\nLSD -h [help and exit]\n");
    printf("         -t [input file with local gene trees]\n");
    printf("         -g [input file with defining groups/populations]\n");
    printf("         -o [output stem name. It will result in two files, one for raw (.lsd) and another for normalized (.lsdnorm) LSD values]\n");
    printf("         -n [number of taxa in local gene trees]\n");
    printf("         -r [outgroup sequence ID, to reroot the tree. Not rerooting if it is already the outgroup])\n");
    printf("\n");
}
/*///////////////////////////////////////////////////////////////////*/
int main(int argc, char *argv[]) {

    char nwkname[1024];
    char groupname[1024];
    char outputname[1024];
    char root[1024];
    
    char **header;
    struct Groups *groups;
    
    int fields;
    int i,j;

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    fprintf(stderr, "\n\nLSD started %d-%d-%d %d:%d:%d, with parameters:\n",tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    for(i=0;i<argc;i++)
      fprintf(stderr,"%s ",argv[i]);
    
    fprintf(stderr,"\n");
    int arg;
    if( argc > 1 ){
        arg = 1;
        while( arg < argc ){
            if( argv[arg][0] != '-' ){
                if(argv[arg][0] == '>')
                    break;
                fprintf(stderr,"\nERROR: Argument should be -%s ?\n", argv[arg]);
                usage();
                exit(1);
            }
            
            switch (argv[arg][1]){
                case 't':
                    arg++;
                    sscanf(argv[arg], "%s",nwkname);
                    break;

                case 'g':
                    arg++;
                    sscanf(argv[arg], "%s",groupname);
                    break;
                    
                case 'o' :
                    arg++;
                    sscanf(argv[arg], "%s",outputname);
                    break;

                case 'n':
                    arg++;
                    sscanf(argv[arg], "%u",&n_taxa);
                    break;

                case 'r':
                    arg++;
                    sscanf(argv[arg], "%s",root);
                    break;
                    
                case 'h' :
                    usage();
                    exit(0);
                    break;
                    
                default:
                    fprintf(stderr,"\nERROR: Unknown option -%c\n",argv[arg][1]);
                    usage();
                    exit(1);
                    break;
            }
            arg++;
        }
    }else {
        usage();
        exit(1);
    }
    
    if (nwkname[0] == '\0'){
        fprintf(stderr,"\nERROR: -t option is missing in your running command\n");
        usage();
        exit(1);
    }
    if (groupname[0] == '\0'){
        fprintf(stderr,"\nERROR: -g option is missing in your running command\n");
        usage();
        exit(1);
    }
    if (outputname[0] == '\0'){
        fprintf(stderr,"\nERROR: -o option is missing in your running command\n");
        usage();
        exit(1);
    }
    if (root[0] == '\0'){
        fprintf(stderr,"\nERROR: -r option is missing in your running command\n");
        usage();
        exit(1);
    }
    if (n_taxa == 0 || n_taxa < 3){
        fprintf(stderr,"\nERROR: -n option is either missing in your running command, or set to less than 3, which makes no sense\n");
        usage();
        exit(1);
    }
    
    n_nodes=2*n_taxa-1;
    n_int_nodes=n_nodes-n_taxa;

    
    fprintf(stderr,"\nReading groups file\n===================\n");
    header=(char **)malloc(sizeof(char *));
    groups=(struct Groups*)malloc(sizeof(struct Groups));
    if (header == NULL || groups == NULL){
        fprintf(stderr,"\tERROR: Cannot allocate memory to read groups file\n");
        exit(0);
    }
    
    
    /*read file defining groups/populations*/
    fields=readgroups2(groupname,&groups,&header);
    if (fields != n_taxa)
        fprintf(stderr,"\tWARN: Number of taxa is not equal to the number of fields in the the groups file: %i vs. %i (some taxa in the local gene tree will not used)\n",n_taxa,fields);

    
    for (i = 0 ;i<n_lines-1;i++){
      if ((groups[i].A0size == 0 && groups[i].A1size==0 && groups[i].A2size==0) || (groups[i].A0size == 0 && groups[i].A1size==0 && groups[i].A2size>0) || (groups[i].A0size == 0 && groups[i].A1size>0 && groups[i].A2size==0) || (groups[i].A0size > 0 && groups[i].A1size>0 && groups[i].A2size>0)){
          fprintf(stderr,"\tERROR: In the groups file, A0 vs (A1 and A2) are mutually exclusive. Line number %i\n",i+1);
          exit(0);
      }
      if ((groups[i].B0size == 0 && groups[i].B1size==0 && groups[i].B2size==0) || (groups[i].B0size == 0 && groups[i].B1size==0 && groups[i].B2size>0) || (groups[i].B0size == 0 && groups[i].B1size>0 && groups[i].B2size==0) || (groups[i].B0size > 0 && groups[i].B1size>0 && groups[i].B2size>0)){
          fprintf(stderr,"\tERROR: In the groups file, B0 vs (B1 and B2) are mutually exclusive. Line number %i\n",i+1);
          exit(0);
      }
    }

    for (i = 0 ;i<n_lines-1;i++){
        fprintf(stderr,"\tComparison %i\n",i);
        if (groups[i].A0size>0){
            fprintf(stderr,"\t\tIndividuals A0: ");
            for (j = 0; j<groups[i].A0size; j++) {
                fprintf(stderr,"%s ",groups[i].A0[j]);
            }
            fprintf(stderr,"\n");
        }
        if (groups[i].A1size>0){
            fprintf(stderr,"\t\tIndividuals A1: ");
            for (j = 0; j<groups[i].A1size; j++) {
                fprintf(stderr,"%s ",groups[i].A1[j]);
            }
            fprintf(stderr,"\n");
        }
        if (groups[i].A2size>0){
            fprintf(stderr,"\t\tIndividuals A2: ");
            for (j = 0; j<groups[i].A2size; j++) {
                fprintf(stderr,"%s ",groups[i].A2[j]);
            }
            fprintf(stderr,"\n");
        }
        if (groups[i].B0size>0){
            fprintf(stderr,"\t\tIndividuals B0: ");
            for (j = 0; j<groups[i].B0size; j++) {
                fprintf(stderr,"%s ",groups[i].B0[j]);
            }
            fprintf(stderr,"\n");
        }
        if (groups[i].B1size>0){
            fprintf(stderr,"\t\tIndividuals B1: ");
            for (j = 0; j<groups[i].B1size; j++) {
                fprintf(stderr,"%s ",groups[i].B1[j]);
            }
            fprintf(stderr,"\n");
        }
        if (groups[i].B2size>0){
            fprintf(stderr,"\t\tIndividuals B2: ");
            for (j = 0; j<groups[i].B2size; j++) {
                fprintf(stderr,"%s ",groups[i].B2[j]);
            }
            fprintf(stderr,"\n");
        }
    }
    
    /*iterate over each local gene tree in nwkname*/
    fprintf(stderr,"\n\nReading tree file, and calculating LSD\n======================================\n");
    read_nwk(nwkname,groups,root,outputname);
    
    /*free allocated memory*/
    for (i = 0 ;i<fields;i++)
        free(header[i]);
    free(header);
    
    for (i = 0 ;i<n_lines-1;i++){
        for (j = 0; j<groups[i].A0size; j++) free(groups[i].A0[j]);
        for (j = 0; j<groups[i].A1size; j++) free(groups[i].A1[j]);
        for (j = 0; j<groups[i].A2size; j++) free(groups[i].A2[j]);
        for (j = 0; j<groups[i].B0size; j++) free(groups[i].B0[j]);
        for (j = 0; j<groups[i].B1size; j++) free(groups[i].B1[j]);
        for (j = 0; j<groups[i].B2size; j++) free(groups[i].B2[j]);
        if (groups[i].A0size>0) free(groups[i].A0);
        if (groups[i].A1size>0) free(groups[i].A1);
        if (groups[i].A2size>0) free(groups[i].A2);
        if (groups[i].B0size>0) free(groups[i].B0);
        if (groups[i].B1size>0) free(groups[i].B1);
        if (groups[i].B2size>0) free(groups[i].B2);
    }
    free(groups);
    fprintf(stderr,"\nFINISHED\n");
   
  return 0;
}
