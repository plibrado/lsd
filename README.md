# **LSD:  Levels of exclusively Shared Differences**#

LSD is a fast and flexible framework to perform genome-wide selection scans, along the internal and external branches of a given population tree. See the original publication for further details.

## **Compiling LSD** ##
As LSD is implemented in the C programming language, it needs to be compiled. If gcc (the C compiler) is installed, open a command-line terminal, and type:

`gcc lsd.c -o lsd -lm && chmod 755 lsd`

This should generate a *lsd* binary ready to be executed. No further installation is required.

## **LSD running options** ##
 Type `./lsd -h` to get an overview of the available options:

> - --h: help
> - --t: input file with local gene trees
> - --g: input file defining groups/populations
> - --o: output stem name. This will provide two files, one for raw (.lsd) and another for normalized (.lsdnorm) LSD values
> - --n: number of taxa in local gene trees
> - --r: outgroup sequence ID, to reroot the tree.

### **Input data files** ###
LSD requires two input data files:

 - The **local gene trees file** is specified through the *-t* option, and contains a collection of local gene trees. Each tree summarises the evolutionary forces operating at a particular locus.
 - The **populations file** is supplied through *-g* option, and used for two purposes. First, to define the population split order. Second, to assign each taxa in the local gene tree to a given population.

#### **Local gene trees (*-t* option)** ####
The *-t* option expects a path pointing to a plain text file, containing multiple local gene trees, one per line and in Newick format ([Newick format, Felsenstein site](http://evolution.genetics.washington.edu/phylip/newicktree.html)).

Local gene tree **requirements**:

 - Non-negative branch lengths
 - Bootstraps are not allowed
 - All trees must have exactly the same taxa (*i.e.* terminal nodes)

The number of taxa in the trees needs to be supplied through the *-n* option. One of these taxa must be the outgroup sequence, used to polarize mutations as ancestral or derived. All trees will be rerooted by *lsd*  using the *-r* option.

#### **Defining groups/populations (*-g* option)** ####

The *-g* option expects a path pointing to a tabulated file, containing as many columns as taxa in the local gene trees. The first line must be the header, containing the IDs for each taxa. These IDs must match taxa labels in the local gene trees.

Each subsequent line specifies a focal branch of the population tree that needs to be analysed. We classify population tree branches following two different criteria:

 - **External** vs. **internal** branches. The former are defined as leading to sampled populations, whereas the latter represents the evolution of populations ancestral to the sampled ones.
 - **Balanced** vs. **unbalanced** branches. Given a focal branch, either external or internal, this can be balanced if its sister branch is external, or unbalanced if its sister branch is internal.

Suppose we aim at analysing a population tree branch that is:

 - **External balanced**. All taxa descending from the focal branch must be flagged as *A0*. All taxa sampled from its sister external branch must be flagged as *B0*. The rest of taxa, including the outgroup, must be flagged as *00*.
 - **External unbalanced**. All taxa descending from the focal branch must be flagged as *A0*. As it is unbalanced, its sister branch further splits into two sub-branches. Taxa descending from sub-branch 1 and sub-branch 2 must be flagged as *B1* or *B2*, respectively. The rest of taxa, including the outgroup, must be flagged as *00*.
 - **Internal balanced**.  As internal, the focal branch splits into two sub-branches. Taxa descending from sub-branch 1 and sub-branch 2 must be flagged as *A1* and *A2*, respectively. All taxa sampled from its sister external branch must be flagged as *B0*. The rest of taxa, including the outgroup, must be flagged as *00*.
 - **Internal unbalanced**.  As internal, the focal branch splits into two sub-branches. Taxa descending from sub-branch 1 and sub-branch 2 must be flagged as *A1* and *A2*, respectively. Similarly, sister individuals descending from sister sub-branches need to be flagged as *B1* and *B2*. The rest of taxa, including the outgroup, must be flagged as *00*.

To wrap up, LSD will interpret taxa starting by *A* as descending from the focal branch, whereas taxa starting by *B* as descending from its sister branch. The numerical suffix indicates whether the branch is external (suffix is *0*) or internal (suffixes are *1* or *2*).

![The four types of focal branches.](img/Doc1.png)


#### **Running example** ####
The *chr22.nwk* file, within this package, contains 6,763 local gene trees. These summarise 6,763 loci distributed along human chromosome 22 (*chr22.ids* indicates their genomic coordinates). Each local gene tree was inferred from 300 chromosomes, representing the chromosomes of 150 diploid individuals from the 1000 Human Genome Project (Phase III). In addition, there is an outgroup sequence labeled as *ANC*. 

The *chr22.pop* population file has three lines, including the header and specifications for studying two focal branches: the first ancestral to all Eurasians, and the second leading to Europeans.

![Population tree analysed. The two targeted branches are color-coded in red (row 2 in *chr22.pop*) and green (row 3 in *chr22.pop*)](img/Doc2.png)

To analyse this data set with LSD, we should therefore include the following options:

    ./lsd -t chr22.nwk -n 301 -r ANC -g chr22.pop -o output22

This should provide two output files, namely *output22_raw* and *output22_norm*, with the lsd and lsdnorm values along the genome.  

## Comments and questions? ##
Contact plibradosanz@gmail.com