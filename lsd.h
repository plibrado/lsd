#define LSD "Detecting signatures of positive selection along defined branches of a population tree using LSD\n"

#ifndef noMacOS
    #define noMacOS	0
#endif

/*//declare struct*/
struct Nodes {
    int ancestor;
    int desc1;
    int desc2;
    char *label;
    double dist;
};
struct Bracket {
    int opened;
    int closed;
};

struct Header{
    char *label;
    int pos;
};

struct Groups{
    char **A0;
    char **B0;
    char **A1;
    char **A2;
    char **B1;
    char **B2;
    int A0size;
    int A1size;
    int A2size;
    int B0size;
    int B1size;
    int B2size;
};

struct Between{
  char *label1;
  char *label2;
  double k;
};
